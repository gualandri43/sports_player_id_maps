# Player ID Mappings

This project aims to provide a wide range of player IDs mappings in use across various internet sites. Aggregating player data, stats, projections, etc. for
analysis across different websites can be an arduous task. The mappings developed in this project aim to make that task much easier by relating the unique identifiers
for each player from each website or data source.

A Python package has been created for easy integration of the ID mappings into a Python project. The package is versioned and contains a built-in copy
of the mappings so that they don't have to be continually downloaded. Projects using other programming languages will have to use the CSV file directly,
or one of the archived formats provided on the [website](https://gualandri43.gitlab.io/sports_player_id_maps).

CLI tools are also provided for retrieving mapping data from various sources for inclusion in the map.

# Data Sources

The player data and mappings are built using a combination of sources. Tools are provided in this repository to aid in retrieving and
matching basic player data from these sources, and then integrating them together. Manual updates are also supported.

Besides the ID values themselves, only the most basic of player information is included, referred to as `meta_*` fields (names, teams, etc.), to aid in the matching process. MLB player info is maintained by the Chadwick Bureau Persons Register and Baseball Reference. NFL player info is maintained by Pro Football Reference.

- Chadwick Bureau Persons Register
- Pro Football Reference
- Baseball Reference

# Downloads

The player ID mapping CSV files may be downloaded directly from this repository. Downloads that have been translated to other formats and compressed are
provided on the [website](https://gualandri43.gitlab.io/sports_player_id_maps).

# Licenses

This project is solely intended to aid in linking players across different services. The use of data specific to each source is subject to
the license terms of each service.

# ID Mapping Data Dictionary

See website for descriptions of each data field provided by the mapping on the [website](https://gualandri43.gitlab.io/sports_player_id_maps).
