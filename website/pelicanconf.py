#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'gualandri43'
SITENAME = 'Sports Player ID Maps'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/New_York'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# path configs
#
#
THEME = 'theme'
#
#THEME_TEMPLATES_OVERRIDES = ['templates']
#
ARTICLE_EXCLUDES = ['pages']
#
PAGE_PATHS = ['pages']
#
PAGE_EXCLUDE = ['generated']


# Blogroll
#LINKS = (('Pelican', 'https://getpelican.com/'),
#         ('Python.org', 'https://www.python.org/'),
#         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
#         ('You can modify those links in your config file', '#'),)

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True