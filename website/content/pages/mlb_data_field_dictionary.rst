MLB Mapping Data Dictionary
######################################

:slug: mlb-data-dictionary
:authors: Dan
:summary: The descriptions of the mlb data fields.
:status: hidden



+--------------------------+-------------------------------------------------------------------------+
| Field                    | Description                                                             |
+==========================+=========================================================================+
| uuid                     | The overall unique ID of the player. This is generated for this project |
|                          | when a new player is added.                                             |
+--------------------------+-------------------------------------------------------------------------+
| local_id                 | A 64 bit integer key derived from the first eight (hex) digits of the   |
|                          | uuid. This is guaranteed to be unique within this list.                 |
+--------------------------+-------------------------------------------------------------------------+
| meta_name_last           | The player's last name.                                                 |
|                          | Source: Chadwick                                                        |
+--------------------------+-------------------------------------------------------------------------+
| meta_name_first          | The player's first name.                                                |
|                          | Source: Chadwick                                                        |
+--------------------------+-------------------------------------------------------------------------+
| meta_name_suffix         | The suffix of a player's name (jr, sr, ...)                             |
|                          | Source: Chadwick                                                        |
+--------------------------+-------------------------------------------------------------------------+
| meta_team                | comma separated list of teams the player has played for.                |
|                          | (planned)                                                               |
+--------------------------+-------------------------------------------------------------------------+
| meta_active_period_start | Season that the player became active.                                   |
|                          | (planned)                                                               |
+--------------------------+-------------------------------------------------------------------------+
| meta_active_period_end   | Season that the player retired or otherwise became inactive.            |
|                          | (planned)                                                               |
+--------------------------+-------------------------------------------------------------------------+
| chadwick                 | The player UUID from the Chadwick register.                             |
|                          | Source: Chadwick                                                        |
+--------------------------+-------------------------------------------------------------------------+
| retrosheet               | The player ID used by retrosheet.                                       |
|                          | Source: Chadwick                                                        |
+--------------------------+-------------------------------------------------------------------------+
| lahman                   | Player IDs from the Lahman database.                                    |
|                          | Source: Lahmand database website.                                       |
+--------------------------+-------------------------------------------------------------------------+
| baseball_reference       | Player IDS from baseball-reference.com.                                 |
|                          | Source: Chadwick                                                        |
+--------------------------+-------------------------------------------------------------------------+
| baseball_reference_minors| Player IDS from baseball-reference.com.                                 |
|                          | Source: Chadwick                                                        |
+--------------------------+-------------------------------------------------------------------------+
| fangraphs                | The player ID used by fangraphs.com.                                    |
|                          | Source: Chadwick, manual                                                |
+--------------------------+-------------------------------------------------------------------------+
| ottoneu                  | (planned)                                                               |
+--------------------------+-------------------------------------------------------------------------+
| mlb                      | The player ID used by mlb.com and MLBAM.                                |
|                          | Source: Chadwick                                                        |
+--------------------------+-------------------------------------------------------------------------+
| cbs                      | (planned)                                                               |
+--------------------------+-------------------------------------------------------------------------+
| nfbc                     | (planned)                                                               |
+--------------------------+-------------------------------------------------------------------------+
| espn                     | (planned)                                                               |
+--------------------------+-------------------------------------------------------------------------+
| yahoo                    | Player IDs for Yahoo Fantasy Sports                                     |
|                          | Source: SportsData.io, MySportsFeeds, manual                            |
+--------------------------+-------------------------------------------------------------------------+
| baseball_prospectus      | (planned)                                                               |
+--------------------------+-------------------------------------------------------------------------+
| rotowire                 | Player IDs for rotowire.com                                             |
|                          | Source: SportsData.io                                                   |
+--------------------------+-------------------------------------------------------------------------+
| fanduel                  | Player IDs for Fanduel DFS.                                             |
|                          | Source: SportsData.io, MySportsFeeds                                    |
+--------------------------+-------------------------------------------------------------------------+
| draftkings               | Player IDs for Draftkings.com DFS.                                      |
|                          | Source: SportsData.io, MySportsFeeds                                    |
+--------------------------+-------------------------------------------------------------------------+
| fantrax                  | (planned)                                                               |
+--------------------------+-------------------------------------------------------------------------+
| mysportsfeeds            | Player IDs from the mysportsfeeds.com API product.                      |
+--------------------------+-------------------------------------------------------------------------+
| sportsdataio             | Player IDs from the sportsdata.io API product.                          |
+--------------------------+-------------------------------------------------------------------------+
| npb                      | (planned)                                                               |
+--------------------------+-------------------------------------------------------------------------+

