Player ID Maps for MLB and NFL
######################################

:slug: home
:authors: Dan
:summary: The home page.
:status: hidden
:save_as: index.html


Connecting datasets across fantasy sports providers or other data providers can be tricky. Some The mappings I
could find were never quite complete enough for what I needed, so I decided to aggregate my own.

These mappings are simply made to link together different data sources, I take no responsibility for the
use or retrieval of said data, which may or may not be subject to license requirements. These maps are
simply made to provide a more centralized way to connect data from site to site.

The player id maps are maintained as csv files in the gitlab repository in the `id_maps` directory. Those csv files may be
used directly, or different formats are available from the download links below.

Mappings for MLB players (`data dictionary <{filename}./mlb_data_field_dictionary.rst>`_)

* `mlb_id_map.csv.zip <{attach}/generated/mlb_id_map.csv.zip>`_
* `mlb_id_map.csv.gz <{attach}/generated/mlb_id_map.csv.gz>`_
* `mlb_id_map.json.zip <{attach}/generated/mlb_id_map.json.zip>`_
* `mlb_id_map.json.gz <{attach}/generated/mlb_id_map.json.gz>`_
* `mlb_id_map.jsonl.zip <{attach}/generated/mlb_id_map.jsonl.zip>`_
* `mlb_id_map.jsonl.gz <{attach}/generated/mlb_id_map.jsonl.gz>`_

