# provide a docker image containing an R environment with all required packages.
#
#

#docker run -it --rm -v C:\Users\gualandrid:/data rocker/tidyverse bash
FROM rocker/tidyverse

# install needed packages
#
RUN R -e 'devtools::install_github("FantasyFootballAnalytics/ffanalytics")'

# default to a R terminal session on start
#CMD ["R"]
