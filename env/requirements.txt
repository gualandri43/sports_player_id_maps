#
# packaging
#
setuptools
wheel
#
# parsing and web scraping
#   simplejson is required by ohmysportsfeedspy
#   sportsipy is a library for scraping www.sports-reference.com
#
lxml
requests
requests_oauthlib
ohmysportsfeedspy
simplejson
sportsipy
#
# data and numerics
#
pandas
#
# environment
#
pylint
ipython
#
# website
#
pelican[markdown]
jinja2
