from setuptools import setup, find_packages
import os

# note:  must gzip compress the .csv mapping files into the
#        sports_player_id_maps/ directory before running this setup.
#
#if not os.path.exists('./sports_player_id_maps/mlb_id_map.csv.gz'):
#    raise ValueError("ID map files must be gzipped into the sports_player_id_maps before build.")
#
#if not os.path.exists('./sports_player_id_maps/nfl_id_map.csv.gz'):
#    raise ValueError("ID map files must be gzipped into the sports_player_id_maps before build.")
#
#if not os.path.exists('./sports_player_id_maps/mlb_id_map.schema.json'):
#    raise ValueError("Schema files must be present in sports_player_id_maps directory before build.")
#
#if not os.path.exists('./sports_player_id_maps/nfl_id_map.schema.json'):
#    raise ValueError("Schema files must be present in sports_player_id_maps directory before build.")


# package definition
#
setup(
    name="sports_player_id_maps",
    version="1.0.2",
    author="Daniel Gualandri",
    packages=find_packages(),
    package_data={
        'id_maps': [
            'mlb_id_map.csv.gz',
            'nfl_id_map.csv.gz',
            'mlb_id_map.schema.json',
            'nfl_id_map.schema.json'
        ]
    },
    install_requires=[
        'lxml',
        'requests',
        'requests_oauthlib',
        'ohmysportsfeedspy',
        'simplejson',
        'sportsipy',
        'pandas'
    ],
    entry_points={
        'console_scripts': [
            'sports_player_id_maps=sports_player_id_maps.cli:main'
        ]
    }
)
