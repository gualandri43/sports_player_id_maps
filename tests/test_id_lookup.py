import unittest
import pandas as pd

from sports_player_id_maps.id_lookup import PlayerIdMap


class IdMapTest(unittest.TestCase):

    def test_get_lookup_dict_mlb(self):

        # Mike Trout
        #   uuid:       8d2e112b-db29-47c9-9af2-e9d473c1b08e
        #   retrosheet: troum001
        #   fangraphs:  10155
        #
        # Mookie Betts
        #   uuid:       79ba14d1-98c5-4761-8ff0-faa3de98c17c
        #   retrosheet: bettm001
        #   fangraphs:  13611
        #

        idMap = PlayerIdMap.create_map('mlb')

        lookup = idMap.get_lookup_dict('retrosheet', 'fangraphs')

        self.assertEqual(
            lookup['troum001'],
            10155
        )

        self.assertEqual(
            lookup['bettm001'],
            13611
        )

    def test_get_player_mapping_from_uuid_mlb(self):

        # Mike Trout
        #   uuid:       8d2e112b-db29-47c9-9af2-e9d473c1b08e
        #   retrosheet: troum001
        #   fangraphs:  10155
        #
        # Mookie Betts
        #   uuid:       79ba14d1-98c5-4761-8ff0-faa3de98c17c
        #   retrosheet: bettm001
        #   fangraphs:  13611
        #

        idMap = PlayerIdMap.create_map('mlb')

        self.assertEqual(
            idMap.get_player_mapping_from_uuid('8d2e112b-db29-47c9-9af2-e9d473c1b08e', 'retrosheet'),
            'troum001'
        )

        self.assertEqual(
            idMap.get_player_mapping_from_uuid(['8d2e112b-db29-47c9-9af2-e9d473c1b08e','79ba14d1-98c5-4761-8ff0-faa3de98c17c'], 'retrosheet'),
            {'8d2e112b-db29-47c9-9af2-e9d473c1b08e':'troum001', '79ba14d1-98c5-4761-8ff0-faa3de98c17c':'bettm001'}
        )

        self.assertEqual(
            idMap.get_player_mapping_from_uuid(['8d2e112b-db29-47c9-9af2-e9d473c1b08e','79ba14d1-98c5-4761-8ff0-faa3de98c17c'], ['retrosheet', 'fangraphs']),
            {'8d2e112b-db29-47c9-9af2-e9d473c1b08e': {
                    'retrosheet':'troum001',
                    'fangraphs': 10155
                },
             '79ba14d1-98c5-4761-8ff0-faa3de98c17c':{
                    'retrosheet': 'bettm001',
                    'fangraphs': 13611
                }
            }
        )

        self.assertEqual(
            idMap.get_player_mapping_from_uuid('8d2e112b-db29-47c9-9af2-e9d473c1b08e', ['retrosheet', 'fangraphs']),
            {'retrosheet':'troum001', 'fangraphs': 10155.0}
        )

    def test_get_player_id_mlb(self):

        # Mike Trout
        #   uuid:       8d2e112b-db29-47c9-9af2-e9d473c1b08e
        #   local_id:   2368606507
        #   chadwick:   f322d40f-e103-4a4f-a17c-f1f69c3f1e40
        #   retrosheet: troum001
        #   fangraphs:  10155
        #

        idMap = PlayerIdMap.create_map('mlb')

        self.assertEqual(
            idMap.get_player_id('8d2e112b-db29-47c9-9af2-e9d473c1b08e', 'uuid', 'local_id'),
            '2368606507'
        )

        self.assertEqual(
            idMap.get_player_id('8d2e112b-db29-47c9-9af2-e9d473c1b08e', 'uuid', 'chadwick'),
            'f322d40f-e103-4a4f-a17c-f1f69c3f1e40'
        )

        self.assertEqual(
            idMap.get_player_id('troum001', 'retrosheet', 'fangraphs'),
            '10155'
        )

        self.assertEqual(
            idMap.get_player_id(10155, 'fangraphs', 'uuid'),
            '8d2e112b-db29-47c9-9af2-e9d473c1b08e'
        )

    def test_convert_pandas_data(self):

        # Mike Trout
        #   uuid:       8d2e112b-db29-47c9-9af2-e9d473c1b08e
        #   retrosheet: troum001
        #   fangraphs:  10155
        #
        # Mookie Betts
        #   uuid:       79ba14d1-98c5-4761-8ff0-faa3de98c17c
        #   retrosheet: bettm001
        #   fangraphs:  13611
        #

        idMap = PlayerIdMap.create_map('mlb')

        df = pd.DataFrame([
            {'uuid': '8d2e112b-db29-47c9-9af2-e9d473c1b08e','fangraphs': 10155},
            {'uuid': '79ba14d1-98c5-4761-8ff0-faa3de98c17c','fangraphs': 13611}
        ])

        df = idMap.convert_pandas_data(df, 'uuid', 'retrosheet')

        self.assertTrue((df.loc[df['uuid'] == '8d2e112b-db29-47c9-9af2-e9d473c1b08e', 'retrosheet'] == 'troum001').all())
        self.assertTrue((df.loc[df['uuid'] == '79ba14d1-98c5-4761-8ff0-faa3de98c17c', 'retrosheet'] == 'bettm001').all())



if __name__ == '__main__':
    unittest.main()
