#!/bin/bash
#
# build the python package
#
# expected working directory: root of the repository
#

# compress maps and move maps and schemas to package.
gzip --keep ./id_maps/*_id_map.csv

# build
python setup.py sdist bdist_wheel

# clean up
rm ./id_maps/*_id_map.csv.gz
