#!/bin/bash
#
# run the tests and generate a coverage report
#
#   use coverage.py library to run the test suite and generate coverage report
#
#   expects the sports_player_id_maps module to be installed
#     pip install ./dist/*.whl
#     pip install coverage
#
#   generates a coverage report in htmlcov/
#
# expected working directory: root of the repository
#

coverage run tests/test_id_lookup.py
coverage report
coverage html
