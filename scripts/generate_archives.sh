#!/bin/bash
#
# generate archive files
#
#  must have the sports_player_id_maps package installed.
#
sports_player_id_maps id_map generate_archive_files csv zip
sports_player_id_maps id_map generate_archive_files csv gzip
sports_player_id_maps id_map generate_archive_files json zip
sports_player_id_maps id_map generate_archive_files json gzip
sports_player_id_maps id_map generate_archive_files jsonl zip
sports_player_id_maps id_map generate_archive_files jsonl gzip
#python sports_player_id_maps/cli.py id_map generate_archive_files xml zip
#python sports_player_id_maps/cli.py id_map generate_archive_files xml gzip

