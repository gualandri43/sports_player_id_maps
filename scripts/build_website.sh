# build the website
#
#   run from repository root
#

if [ -d "./dist" ]
then
    # use new package if possible
    pip install ./dist/*.whl
else
    # use fall back to previous version
    pip install --extra-index-url https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.example.com/api/v4/projects/${CI_PROJECT_ID}/packages/pypi/simple --no-deps sports_player_id_maps
fi

pip install pelican[markdown]
/bin/bash ./scripts/generate_archives.sh
mv *_id_map.*.zip ./website/content/generated/
mv *_id_map.*.gz ./website/content/generated/
cd ./website
make publish
