'''
CLI entrypoint.
'''

import argparse

import sports_player_id_maps.tools.archive_files as archive_files
import sports_player_id_maps.tools.updater as updater
import sports_player_id_maps.tools.mlb_scrapers as mlb_scrapers


def build_parser():
    '''
        build the argument parser.
    '''

    parser = argparse.ArgumentParser('mapping_tools', description='Tools for editing and working with the player ID maps.')

    subparsers = parser.add_subparsers()

    
    # map maintenance functionality
    id_map = subparsers.add_parser('id_map')
    id_map_subparsers = id_map.add_subparsers(title='ID Mapping Commands')
    archive_files.cli_arguments(id_map_subparsers)
    updater.cli_arguments(id_map_subparsers)

    # mlb data scrapers
    mlb_scrape = subparsers.add_parser('mlb_scrape')
    mlb_scrape_subparsers = mlb_scrape.add_subparsers(title='MLB Player Data Scraping Commands')
    mlb_scrapers.cli_arguments(mlb_scrape_subparsers)

    # nfl data scrapers
    #scrape_nfl = subparsers.add_parser('scrape_nfl')

    return parser

def main():
    '''
        entrypoint
    '''

    # parse and execute
    #
    parser = build_parser()
    args = parser.parse_args()
    args.func(args)

if __name__ == '__main__':
    main()