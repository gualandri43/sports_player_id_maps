'''
    Helper classes and functions to look up a players ID across sources. Uses the
    csv files in id_maps/.
'''

import pkg_resources
import gzip
import io
import json
import xml.etree.cElementTree as ET
import pandas as pd


class IdMapSchema:
    '''Data schema for the ID map.'''

    def __init__(self, sport):

        if sport not in PlayerIdMap.sports:
            raise ValueError("Invalid sport.")

        if sport == 'mlb':
            with pkg_resources.resource_stream('id_maps', 'mlb_id_map.schema.json') as f:
                schema = json.load(f)

        if sport == 'nfl':
            with pkg_resources.resource_stream('id_maps', 'nfl_id_map.schema.json'):
                schema = json.load(f)

        self._schema = dict()
        for _field in schema:

            if schema[_field] == 'integer':
                self._schema[_field] = pd.Int64Dtype()

            if schema[_field] == 'string':
                self._schema[_field] = pd.StringDtype()

    def save(self):
        '''Export schema to JSON string.'''

        export = dict()

        for _field in self._schema:

            if isinstance(self._schema[_field], pd.Int64Dtype):
                export[_field] = 'integer'

            if isinstance(self._schema[_field], pd.StringDtype):
                export[_field] = 'string'

        return json.dumps(export)

    @property
    def dataMap(self):
        '''Get the dictionary data mapping.'''
        return self._schema

    def add(self, fieldName, dtype):
        '''
        Add a field name to schema.

        fieldName       string name of new field.
        dtype           pandas or numpy dtype
        '''

        if fieldName in self._schema:
            raise ValueError("Field name already taken.")
        else:
            self._schema[fieldName] = dtype

class PlayerIdMap:

    # all valid sports
    #
    sports = [
        'mlb',
        'nfl'
    ]

    # all valid nfl map column names and the corresponding datatypes
    #
    #nflCols = {
    #    'uuid': pd.StringDtype(),
    #    'local_id': pd.Int64Dtype(),
    #    'nfl': pd.StringDtype(),
    #    'espn': pd.Int64Dtype(),                      # TBD
    #    'yahoo': pd.Int64Dtype(),
    #    'fantasypros': pd.Int64Dtype(),               # TBD
    #    'fantasypros_slug': pd.StringDtype(),
    #    'numberfire_slug': pd.StringDtype(),
    #    'pro_football_reference': pd.StringDtype(),
    #    'fanduel': pd.Int64Dtype(),                   # TBD
    #    'draftkings': pd.Int64Dtype()                 # TBD
    #}


    @classmethod
    def create_map(cls, sport):
        '''Factory function to load the built-in player ID mappings.'''

        schema = IdMapSchema(sport)

        if sport == 'mlb':
            with pkg_resources.resource_stream('id_maps', 'mlb_id_map.csv.gz') as fmap:
                with gzip.open(fmap) as fzip:
                    df = pd.read_csv(fzip, header=0, usecols=schema.dataMap.keys(), dtype=schema.dataMap)
                    mapping = df.set_index('uuid')

        if sport == 'nfl':
            with pkg_resources.resource_stream('id_maps', 'nfl_id_map.csv.gz') as fmap:
                with gzip.open(fmap) as fzip:
                    df = pd.read_csv(fzip, header=0, usecols=schema.dataMap.keys(), dtype=schema.dataMap)
                    mapping = df.set_index('uuid')

        return cls(sport, mapping, schema)

    @classmethod
    def build_map_basename(cls, sport):
        '''
            Create the string basename of the mapping file.
        '''

        if sport not in cls.sports:
            raise ValueError("Invalid sport requested.")

        return '{0}_id_map'.format(sport)

    @property
    def schema(self):
        '''The IdMapSchema instance defining the data fields.'''
        return self._schema

    @property
    def dataframe(self):
        '''Get full ID map as a Pandas dataframe.'''
        return self._idMap

    @property
    def csv(self):
        '''Get a CSV string representation of the ID map.'''

        outString = io.StringIO()
        self._idMap.to_csv(outString, header=True, index=True, na_rep='')
        return outString.getvalue()

    @property
    def json(self):
        '''Get a JSON string representation of the ID map.'''

        outString = io.StringIO()
        self._idMap.to_json(outString, orient='records')
        return outString.getvalue()

    @property
    def jsonl(self):
        '''Get a JSON lines string representation of the ID map.'''

        outString = io.StringIO()
        self._idMap.to_json(outString, orient='records', lines=True)
        return outString.getvalue()

    @property
    def xml(self):
        '''Get a XML string representation of the ID map.'''

        # xml
        #
        # <mlb_id_map>
        #   <player uuid=...>
        #     <fangraphs>
        #       ...
        #     </fangraphs>
        #   </player>
        # </mlb_id_map>

        root = ET.Element(PlayerIdMap.build_map_basename(self.sport))
        for _idx,_data in self._idMap.iterrows():
            playerElem = ET.SubElement(root, "player", attrib={'uuid': _idx})

            for _col,_val in _data.iteritems():
                mappingElem = ET.SubElement(playerElem, _col)
                mappingElem.text = str(_val)

        return ET.tostring(root)

    @property
    def fieldnames(self):
        '''List of the current mapping data fields.'''

        return list(self._schema.dataMap.keys())

    def __init__(self, sport, mapping, schema):
        '''

        sport       The sport the mappings are for
        mapping     A pandas dataframe where each row represents a player.
                    Columns represent the player ID or meta info.
        schema      An instance of IdMapSchema
        '''

        if sport not in self.sports:
            raise ValueError("sport is not a recognized target: {0}".format(self.sports))

        if not isinstance(schema, IdMapSchema):
            raise ValueError("schema must be a IdMapSchema object.")

        if not isinstance(mapping, pd.DataFrame):
            raise ValueError("schema must be a DataFrame object.")

        for _col in mapping.columns:
            if _col not in schema.dataMap:
                raise ValueError("Column not in given schema: {0}".format(_col))

        for _field in schema.dataMap:
            if _field not in mapping.columns and _field not in mapping.index.names:
                raise ValueError("Field not in given dataframe: {0}".format(_field))

        self.sport = sport
        self._schema = schema
        self._idMap = mapping

    def get_lookup_dict(self, keyField, valueField):
        '''
            Get a lookup dictionary with data field keyField as the dictionary key and
            valueField data field as the associated value.
        '''

        ids = self._idMap.reset_index()

        if keyField not in ids.columns or valueField not in ids.columns:
            raise ValueError("field names must be valid data fields in map.")

        ids = ids.loc[ids[keyField].notna()]
        ids.set_index(keyField, inplace=True)
        return ids[valueField].to_dict()

    def get_player_mapping_from_uuid(self, playerUuid, lookupField):
        '''
            lookup a player by their overall UUID and return the given
            data field.

            playerUuid and lookupField may be either single strings or lists of values to return.

            if inputs are single value, return a single value.
            if only lookupField is a list, return a dictionary with key=field, value=id
            if only playerUuid is a list, return a dictionary with key=playerUuid, value=id
            if both inputs are a list, return a dictionary with key=playerUuid, value={field: id}
        '''

        requested = self._idMap.loc[playerUuid, lookupField]

        if not isinstance(playerUuid, list) and not isinstance(lookupField, list):
            return str(requested)

        elif isinstance(playerUuid, list) and not isinstance(lookupField, list):
            return requested.to_dict()

        elif not isinstance(playerUuid, list) and isinstance(lookupField, list):
            return requested.to_dict()

        elif isinstance(playerUuid, list) and isinstance(lookupField, list):
            return requested.to_dict(orient='index')

    def get_player_id(self, lookupId, lookupField, targetField):
        '''
            get a player id for source field targetField with known lookupId in
            data field lookupField.
        '''

        ids = self._idMap.reset_index()
        requested = ids.loc[ids[lookupField] == lookupId, targetField]
        return str(requested.values[0])

    def convert_pandas_data(self, data, lookupField, targetField):
        '''
            Add a column to a pandas dataframe with the converted ID values.
            Returns a copy of the dataframe with the new column added.

            data            a pandas dataframe
            lookupField     the name of the dataframe column to lookup in the ID map.
                            this name must be a compatible data field name from the
                            ID map in this package.
            targetField     the name of the data field to convert to. adds a
                            column to the dataframe `data` with this name.
        '''

        ids = self._idMap.reset_index()

        if lookupField not in ids.columns or targetField not in ids.columns:
            raise ValueError("field names must be valid data fields in map.")

        ids.set_index(lookupField, inplace=True)
        ids = ids.loc[:, targetField].dropna()

        return data.merge(ids, how='left', left_on=lookupField, right_index=True)
