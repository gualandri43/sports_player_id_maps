'''
Generate archive mappings files.
'''

import argparse
import zipfile
import gzip
import os

from sports_player_id_maps.id_lookup import PlayerIdMap


def generate_archive_files(sport, fileformat='csv', compression='zip', basedir='.'):
    '''
        compression         either zip or gzip
        fileformat          csv, json, jsonl, xml

        Convert ID mapping file to the following file types:
            .csv.zip
            .csv.gz
            .json.zip
            .json.gz
            .jsonl.zip
            .jsonl.gz
            .xml.zip
            .xml.gz
    '''

    idMap = PlayerIdMap.create_map(sport)
    archiveNameBase = os.path.join(basedir, PlayerIdMap.build_map_basename(sport))
    fileNameBase = PlayerIdMap.build_map_basename(sport)

    # csv
    #
    if fileformat == 'csv':

        csvString = idMap.csv

        if compression == 'zip':
            with zipfile.ZipFile(archiveNameBase + '.csv.zip', 'w', compression=zipfile.ZIP_DEFLATED, compresslevel=9) as zipArchive:
                zipArchive.writestr(fileNameBase + '.csv', csvString)

        if compression == 'gzip':
            with gzip.open(archiveNameBase + '.csv.gz', 'wt') as gzipFile:
                gzipFile.write(csvString)

    # json
    #
    if fileformat == 'json':

        jsonString = idMap.json

        if compression == 'zip':
            with zipfile.ZipFile(archiveNameBase + '.json.zip', 'w', compression=zipfile.ZIP_DEFLATED, compresslevel=9) as zipArchive:
                zipArchive.writestr(fileNameBase + '.json', jsonString)

        if compression == 'gzip':
            with gzip.open(archiveNameBase + '.json.gz', 'wt') as gzipFile:
                gzipFile.write(jsonString)

    # jsonl
    #
    if fileformat == 'jsonl':

        jsonlString = idMap.jsonl

        if compression == 'zip':
            with zipfile.ZipFile(archiveNameBase + '.jsonl.zip', 'w', compression=zipfile.ZIP_DEFLATED, compresslevel=9) as zipArchive:
                zipArchive.writestr(fileNameBase + '.json', jsonlString)

        if compression == 'gzip':
            with gzip.open(archiveNameBase + '.jsonl.gz', 'wt') as gzipFile:
                gzipFile.write(jsonlString)


    if fileformat == 'xml':

        xmlString = idMap.xml

        if compression == 'zip':
            with zipfile.ZipFile(archiveNameBase + '.xml.zip', 'w', compression=zipfile.ZIP_DEFLATED, compresslevel=9) as zipArchive:
                zipArchive.writestr(fileNameBase + '.xml', xmlString)

        if compression == 'gzip':
            with gzip.open(archiveNameBase + '.xml.gz', 'wb') as gzipFile:
                gzipFile.write(xmlString)

def cli_generate_archive_files(args):
    '''
        cli_generate_archive_files cli command
    '''

    generate_archive_files('mlb', fileformat=args.fileFormat, compression=args.compression)
    #generate_archive_files('nfl', fileformat=args.fileFormat, compression=args.compression)

def cli_arguments(parser):
    '''
        add parsers for each command. parser can be a ArgumentParser or a subparsers object
        created by add_subparsers().
    '''

    # add the various commands and their arguments
    #

    generate_archive_files = parser.add_parser('generate_archive_files')
    generate_archive_files.add_argument('fileFormat', choices=['csv', 'json', 'jsonl', 'xml'])
    generate_archive_files.add_argument('compression', choices=['zip', 'gzip'])
    generate_archive_files.set_defaults(func=cli_generate_archive_files)
