'''
Mapping file updater tools.
'''

import argparse
import uuid
import os
import pandas as pd

from sports_player_id_maps.id_lookup import PlayerIdMap

class MappingFileUpdater:
    '''
        Class to facilitate updating mappings in the ID map. Always uses the
        current released version of the map as a base.
    '''

    def __init__(self, sport):

        if sport not in PlayerIdMap.sports:
            raise ValueError('Invalid sport requested.')

        self.sport = sport

    def _check_series(self, existingMap, other):
        '''
            existingMap is a PlayerIdMap object to compare other to.
            validates that the name of the series and the index name are valid.
        '''

        if not isinstance(other, pd.Series):
            raise ValueError('other must be a Series object.')

        if not isinstance(existingMap, PlayerIdMap):
            raise ValueError('existingMap must be a PlayerIdMap object.')


        if other.name not in existingMap.fieldnames:
            raise ValueError('The name of the Series must be a valid data field name ({0}).'.format(existingMap.fieldnames))

        if other.index.name not in existingMap.fieldnames:
            raise ValueError('The name of the Series index must be a valid data field name ({0}).'.format(existingMap.fieldnames))

        if other.dtype != existingMap.schema.dataMap[other.name]:
            raise ValueError('The dtype of other must be {0}'.format(existingMap.schema.dataMap[other.name]))

    def _generate_local_integer_id(self, playerUuid):
        '''
            Generate the 64 bit integer to use as a local id.

            Use the first 8 hex digits of UUID that converts UUID to
            a 64 bit integer.
        '''

        # int(x, 16) converts a string assuming base-16 number (hexadecimal)
        #   full uuid hex representation is a 32 character string (uuid can be represented as 128 bit integer)
        #   first 8 digits will make a more manageable key that can be reproduced from UUID
        #   could also get from uuid.hex[:8]
        #

        if not isinstance(playerUuid, uuid.UUID):
            raise ValueError('playerUuid must be a UUID.')

        return int(playerUuid.hex[:8], 16)

    def save_mapping(self, idMap, outDir='.'):
        '''
            save the new id map and associated schema to a csv file
            that can be used to replace the official csv map in the repository.
        '''

        outFilePath = os.path.join(
            outDir, '{0}.csv'.format(PlayerIdMap.build_map_basename(self.sport))
        )

        outSchemaPath = os.path.join(
            outDir, '{0}.schema.json'.format(PlayerIdMap.build_map_basename(self.sport))
        )

        with open(outFilePath, 'w', newline='') as f:
            f.write(idMap.csv)

        with open(outSchemaPath, 'w') as f:
            f.write(idMap.schema.save())

    def calculate_new_entry_ids(self, existingMap, count=1):
        '''
            calculates a new UUID and local integer ID. Uses UUID version 4.

            outside cross references should be stored using UUIDs if possible to preserve consistency.

            existingMap is a PlayerIdMap object to check new entries for uniqueness.

            returns list of length count of 2-tuples (uuid, local int ID)
        '''
        #
        # check to make sure int IDs remain unique
        #

        existing = set(existingMap.dataframe['local_id'].values)

        ids = list()
        while len(ids) < count:
            newUuid = uuid.uuid4()
            newIntId = self._generate_local_integer_id(newUuid)

            # only add if new integer id is unique
            #
            if newIntId not in existing:
                existing.add(newIntId)
                ids.append((newUuid, newIntId))

        return ids

    def compare_with_existing_mappings(self, existingMap, other):
        '''
            existingMap is a PlayerIdMap object to compare other to.
            other is a Series indexed by the merge key, name
            corresponding to the field to update.

            returns a 4-tuple of Series objects
                1 - ID mappings in other that don't exist in current map
                2 - ID mappings in other that exist in current map but are NULL (need to be updated)
                3 - ID mappings in other that exist in current map but map to a different value than the current map (conflicts)
                4 - ID mappings in other that exist in current map and are equal (no updates necessary)
        '''

        self._check_series(existingMap, other)

        # isolate the relevant Series and get a common index
        #
        newMap = other.dropna()
        currentMap = existingMap.dataframe.set_index(newMap.index.name)
        currentMap = currentMap.loc[:, other.name]

        # new entries that don't exist at all in idMap
        #
        newEntries = newMap.loc[newMap.index.difference(currentMap.index)]

        # get values in indexes that are the same between new and existing
        #
        intersectionIdx = newMap.index.intersection(currentMap.index)

        # entries that exist in idMap but are different (includes nas)
        #
        nullEntries = newMap.loc[currentMap.loc[intersectionIdx].isnull().index]
        notNulls = newMap.loc[newMap.loc[intersectionIdx].index.difference(nullEntries.index)]

        if len(notNulls) > 0:
            diffEntries = newMap.loc[newMap.loc[notNulls.index] != currentMap.loc[notNulls.index]]
        else:
            diffEntries = notNulls.copy()

        # entries that exist in idMap and are equal
        #
        if len(notNulls) > 0:
            equalEntries = newMap.loc[newMap.loc[notNulls.index] == currentMap.loc[notNulls.index]]
        else:
            equalEntries = notNulls.copy()

        # add names of series to match other
        #
        newEntries.name = other.name
        nullEntries.name = other.name
        diffEntries.name = other.name
        equalEntries.name = other.name

        return (newEntries, nullEntries, diffEntries, equalEntries)

    def update_mapping(self, existingMap, other, overwrite=False):
        '''
            existingMap is a PlayerIdMap object to update.
            other is a Series indexed by the merge key, name
            corresponding to the field to update.

            overwrite       True will overwrite the current mapping if it exists. Only use
                            True if you are sure you want to modify existing values, not just
                            add new ones.
        '''

        self._check_series(existingMap, other)
        other.dropna(inplace=True)

        # change index to join field (the index of other)
        updateFrame = existingMap.dataframe.reset_index().set_index(other.index.name)

        # find index set of rows to update.
        commonIdx = updateFrame.index.intersection(other.index)

        if overwrite:
            # update all rows with new value
            updateFrame.loc[commonIdx, other.name] = other.loc[commonIdx]
        else:
            # only update null rows in target column
            nulls = updateFrame.loc[commonIdx, other.name].isnull()
            nulls = nulls.loc[nulls]
            updateFrame.loc[nulls.index, other.name] = other.loc[nulls.index]

        updateFrame = updateFrame.reset_index().set_index('uuid')
        existingMap.dataframe.loc[:, other.name] = updateFrame.loc[:, other.name]

    def add_mapping(self, existingMap, other):
        '''
            This method adds new mapping rows to an PlayerIdMap.

            existingMap is a PlayerIdMap object to update.
            other is a Series with no index. If the name of the Series is not
            in the existingMap's schema, a new one will be created. New UUIDs will be generated
            for each row and the given information assigned to that new entry.
        '''

        # input validation
        #
        if not isinstance(other, pd.Series):
            raise ValueError('other must be a Series')

        self._check_series(existingMap, other)


        # validate that values in other don't exist
        #
        existanceCheck = other.isin(existingMap.dataframe[other.name])
        if existanceCheck.any():

            raise ValueError('Mappings in {0} already exists in map: {1}'.format(
                    other.name,
                    ','.join(other.loc[existanceCheck].values)
                )
            )

        # generate new UUIDs for each new mapping and add info
        #
        newRecs = list()
        newIds = self.calculate_new_entry_ids(existingMap, count=len(other))

        rowCount = 0
        for _idx,_val in other.iteritems():
            rec = dict()
            rec['uuid'] = newIds[rowCount][0]
            rec['local_id'] = newIds[rowCount][1]
            rec[other.name] = _val

            rowCount += 1
            newRecs.append(rec)

        newEntries = pd.DataFrame(newRecs)
        newEntries.set_index('uuid', inplace=True)

        existingMap.dataframe = existingMap.dataframe.append(newEntries)



def cli_generate_patch(args):
    '''
        generate_patch command

        outputs csv files for
            new.csv             mappings that did not match any entries in the join field and may need new entries.
            toUpdate.csv        update canditates that match on the join field be are undefined in current map.
            conflicts.csv       candidates that conflict with an existing value in the current map.
    '''

    #args.newMappingFile
    #args.joinField
    #args.sport
    #

    existingMap = PlayerIdMap.create_map(args.sport)

    # remove entries in joinField that are empty before processing
    #
    newMappings = pd.read_csv(args.newMappingFile)
    newMappings = newMappings.loc[newMappings[args.joinField].notnull()]
    newMappings.loc[:, args.joinField] = newMappings[args.joinField].astype(existingMap.schema.dataMap[args.joinField])
    newMappings.set_index(args.joinField, inplace=True)
    updater = MappingFileUpdater(args.sport)

    newData = pd.DataFrame()
    toUpdate = pd.DataFrame()
    conflicts = pd.DataFrame()
    unchanged = pd.DataFrame()

    for _col in newMappings.columns:

        seriesToCheck = newMappings[_col]

        # add a new schema field if needed
        if _col not in existingMap.schema.dataMap:

            # try to infer datatype (either string or integer)
            try:
                seriesToCheck = seriesToCheck.astype(pd.Int64Dtype())

            except TypeError:
                seriesToCheck = seriesToCheck.astype(pd.StringDtype())

            # add schema field and an empty column
            existingMap.schema.add(_col, seriesToCheck.dtype)
            existingMap.dataframe.loc[:, _col] = pd.Series(
                [pd.NA]*len(existingMap.dataframe),
                name=seriesToCheck.name,
                dtype=seriesToCheck.dtype
            )

        else:
            seriesToCheck = seriesToCheck.astype(existingMap.schema.dataMap[_col])

        newEntries, nullEntries, diffEntries, equalEntries = updater.compare_with_existing_mappings(existingMap, seriesToCheck)

        newData.loc[:, _col] = newEntries
        toUpdate.loc[:, _col] = nullEntries
        conflicts.loc[:, _col] = diffEntries
        unchanged.loc[:, _col] = equalEntries

    print('New entries: {0}'.format(len(newData)))
    print('Entries to update: {0}'.format(len(toUpdate)))
    print('Conflicts detected: {0}'.format(len(conflicts)))
    print('Already present: {0}'.format(len(unchanged)))

    newData.to_csv('newData.csv')
    toUpdate.to_csv('toUpdate.csv')
    conflicts.to_csv('conflicts.csv')

def cli_update(args):
    '''
        update command

        Updates mappings in the ID map with new values.
    '''

    #args.newMappingFile
    #args.joinField
    #args.sport
    #args.overwriteConflicting

    updatedMap = PlayerIdMap.create_map(args.sport)     # start with existing map
    newMappings = pd.read_csv(args.newMappingFile, index_col=args.joinField)
    updater = MappingFileUpdater(args.sport)


    for _col in newMappings.columns:
        print('Updating {0} mappings, joined on the {1} field.'.format(_col, args.joinField))

        seriesToAdd = newMappings[_col]

        # add a new schema field if needed
        if _col not in updatedMap.schema.dataMap:

            # try to infer datatype (either string or integer)
            try:
                seriesToAdd = seriesToAdd.astype(pd.Int64Dtype())

            except TypeError:
                seriesToAdd = seriesToAdd.astype(pd.StringDtype())

            # add schema field
            updatedMap.schema.add(_col, seriesToAdd.dtype)
            updatedMap.dataframe.loc[:, _col] = pd.Series(
                [pd.NA]*len(updatedMap.dataframe),
                name=seriesToAdd.name,
                dtype=seriesToAdd.dtype
            )

        else:
            seriesToAdd = seriesToAdd.astype(updatedMap.schema.dataMap[_col])

        # update the mappings.
        try:
            updater.update_mapping(updatedMap, seriesToAdd, overwrite=args.overwriteConflicting)

        except ValueError as inpErr:
            print('Error updating {0} mappings: {1}'.format(_col, str(inpErr)))

    updater.save_mapping(updatedMap)

def cli_add(args):
    '''
        add command

        Add a new set of mappings to the ID map.
    '''

    #args.newMappingFile
    #args.sport

    newMappings = pd.read_csv(args.newMappingFile)
    updater = MappingFileUpdater(args.sport)
    updatedMap = PlayerIdMap.create_map(args.sport)     # start with existing map

    for _col in newMappings.columns:
        seriesToAdd = newMappings[_col]

        print('Adding {0} new mappings.'.format(len(newMappings)))

        # add a new schema field if needed
        if _col not in updatedMap.schema.dataMap:

            # try to infer datatype (either string or integer)
            try:
                seriesToAdd = seriesToAdd.astype(pd.Int64Dtype())

            except TypeError:
                seriesToAdd = seriesToAdd.astype(pd.StringDtype())

            # add schema field
            updatedMap.schema.add(_col, seriesToAdd.dtype)

        else:
            seriesToAdd = seriesToAdd.astype(updatedMap.schema.dataMap[_col])

        try:
            updater.add_mapping(updatedMap, newMappings)
            updater.save_mapping(updatedMap)

        except ValueError as inpErr:
            print('Error adding new mappings: {0}'.format(str(inpErr)))

def _add_argument_helper(parser, argumentParameters):
    '''
        add an argument to a parser from a dictionary of
        definitions.
    '''

    params = argumentParameters.copy()
    parser.add_argument(params.pop('name'), **params)

def cli_arguments(parser):
    '''
        add parsers for each command. parser can be a ArgumentParser or a subparsers object
        created by add_subparsers().
    '''

    # define command line options so they can be reused.
    #
    newMappingFile = {
        'name': '--newMappingFile',
        'dest': 'newMappingFile',
        'action': 'store',
        'help': 'The path to the file containing the candidate mappings.'
    }

    sport = {
        'name': '--sport',
        'dest': 'sport',
        'action': 'store',
        'choices': ['nfl', 'mlb'],
        'help': 'The sport to apply the candidate mappings to.'
    }

    joinField = {
        'name': '--joinField',
        'dest': 'joinField',
        'action': 'store',
        'help': 'Field in the supplied candidate mapping table that will be used for matching.'
    }

    overwriteConflicting = {
        'name': '--overwriteConflicting',
        'dest': 'overwriteConflicting',
        'action': 'store_true',
        'help': 'Overwrite existing mapped ids present in the candidate mapping set.'
    }


    # add the various commands and their arguments
    #

    add = parser.add_parser('add')
    _add_argument_helper(add, newMappingFile)
    _add_argument_helper(add, sport)
    add.set_defaults(func=cli_add)

    update = parser.add_parser('update')
    _add_argument_helper(update, newMappingFile)
    _add_argument_helper(update, sport)
    _add_argument_helper(update, joinField)
    _add_argument_helper(update, overwriteConflicting)
    update.set_defaults(func=cli_update)

    generate_patch = parser.add_parser('generate_patch')
    _add_argument_helper(generate_patch, newMappingFile)
    _add_argument_helper(generate_patch, sport)
    _add_argument_helper(generate_patch, joinField)
    generate_patch.set_defaults(func=cli_generate_patch)
