'''
logging setup
'''

import logging

def set_up_console_logger(loggerName):
    '''
    Convenience function to add a standard formatted console log handler to
    a given loggerName. Run once, and the loggerName will remain set up correctly.

    retrieve the logger from a script or module with logging.getLogger(loggerName)

    Only run once per loggerName
    '''

    logger = logging.getLogger(loggerName)

    formatter = logging.Formatter('[%(asctime)s | %(name)s] [%(levelname)s]  %(message)s')
    console = logging.StreamHandler()
    console.setFormatter(formatter)

    logger.addHandler(console)
    logger.setLevel(logging.INFO)
