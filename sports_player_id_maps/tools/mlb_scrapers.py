'''
Scrapers for downloading MLB player data.
'''

import uuid
import io
import datetime
import os
import requests
import pandas as pd
import numpy as np
import ohmysportsfeedspy

class ChadwickBureauRegister:
    '''
        Chadwick Bureau Register ID mapping from Github
    '''
    # Notes on source data.
    #
    #   key_uuid:           The primary key, providing the most stable reference to a person.
    #                       Guaranteed not to be re-issued to a different person in the future.
    #   key_person:         The first eight (hex) digits of the key_uuid.
    #                       It is guaranteed that this is unique at any given time.
    #                       However, should a person's record be withdrawn from the register,
    #                       the same key_person may reappear referencing a different person in the future.
    #   key_retro:          The person's Retrosheet identifier.
    #   key_mlbam:          The person's identifier as used by MLBAM (for example, in Gameday).
    #   key_bbref:          The person's identifier on Major League pages on baseball-reference.com.
    #   key_bbref_minors:   The person's identifier on minor league and Negro League pages on baseball-reference.com.
    #   key_fangraphs:      The person's identifier on fangraphs.com (Major Leaguers). As fangraphs uses BIS identifiers,
    #                       this can also be used to match up people to BIS data.
    #   key_npb:            The person’s identifier on the NPB official site, npb.or.jp.
    #   key_sr_nfl:         The person's identifier on pro-football-reference.com
    #   key_sr_nba:         The person's identifier on basketball-reference.com
    #   key_sr_nhl:         The person's identifier on hockey-reference.com
    #   key_findagrave:     The identifier of the person's memorial on findagrave.com
    #
    #   player info:
    #       name_last
    #       name_first
    #       name_given
    #       name_suffix
    #       name_matrilineal
    #       name_nick
    #       birth_year
    #       birth_month
    #       birth_day
    #       death_year
    #       death_month
    #       death_day

    downloadUrl = 'https://github.com/chadwickbureau/register/blob/master/data/people.csv?raw=true'

    headingMapping = {
        'key_uuid': 'chadwick',
        #'key_person': None,
        'key_retro': 'retrosheet',
        'key_mlbam': 'mlb',
        'key_bbref': 'baseball_reference',
        'key_bbref_minors': 'baseball_reference_minors',
        'key_fangraphs': 'fangraphs',
        'key_npb': 'npb',
        'name_last': 'name_last',
        'name_first': 'name_first',
        'name_suffix': 'name_suffix'
    }

    def get(self):
        '''
            Download data from the Register and format as needed.
        '''

        # request data from Github repo
        #
        response = requests.get(
            self.downloadUrl,
            headers = {
                "Accept-Encoding": "gzip, deflate, br"
            }
        )

        # load response data into a dataframe
        #
        chdwckFile = io.StringIO(response.text)
        chdwckFile.seek(0)
        df = pd.read_csv(chdwckFile, dtype='string', header=0, usecols=self.headingMapping.keys())

        # rename columns
        #
        df.rename(columns=self.headingMapping, inplace=True)

        # make uuid field a uuid.UUID object and make it the index
        #
        df = df.astype({'chadwick': 'object'})
        df.loc[:, 'chadwick'] = df.loc[:, 'chadwick'].apply(lambda x: uuid.UUID(x, version=None))
        return df

class LahmanDatabase:
    '''
        Lahman Database id mappings.

        Mirror hosted by the chadwick bureau project.
    '''
    # Notes on source data.
    #
    #   playerID        lahmand database id
    #   retroID         id from retrosheet
    #   bbrefID         id from baseball reference

    downloadUrl = 'https://raw.githubusercontent.com/chadwickbureau/baseballdatabank/master/core/People.csv'

    headingMapping = {
        'playerID': 'lahman',
        'retroID': 'retrosheet',
        'bbrefID': 'baseball_reference'
    }

    def get(self):
        '''
            Download mapping data and format as needed.

            return a dataframe of the lahman database mapping from Github.
        '''

        # request data from Github repo
        #
        # download csv text file as string file object
        #   this is public data, no need to verify ssl
        #
        response = requests.get(
            self.downloadUrl,
            headers = {
                "Accept-Encoding": "gzip, deflate, br"
            }
        )

        # load response data into a dataframe
        #
        lahmanFile = io.StringIO(response.text)
        lahmanFile.seek(0)
        df = pd.read_csv(lahmanFile, dtype='string', header=0, usecols=self.headingMapping.keys())

        # rename columns
        #
        df.rename(columns=self.headingMapping, inplace=True)

        return df

class SmartFantasyBaseball:
    '''
        www.smartfantasybaseball.com ID mapping.
    '''

    # Notes on source data.
    #
    #   IDPLAYER            local player ID
    #   PLAYERNAME          player name
    #   TEAM                team of the player
    #   ALLPOS              eligible positions
    #   IDFANGRAPHS         fangraphs id
    #   MLBID               MLB.com
    #   CBSID               CBS
    #   RETROID             retrosheet
    #   BREFID              baseball reference
    #   NFBCID              nfbc
    #   ESPNID              ESPN,
    #   KFFLNAME            KFFL (name, not id?)
    #   DAVENPORTID
    #   BPID                baseball prospectus
    #   YAHOOID             yahoo id
    #   MSTRBLLNAME
    #   FANTPROSNAME        fantasy pros name (can get id by lowercase)
    #   ROTOWIREID          rotowire
    #   FANDUELID           fanduel,
    #   DRAFTKINGSNAME      draftkings name
    #   OTTONEUID           ottoneu
    #   HQID                hq
    #   FANTRAXID           fantrax
    #   RAZZBALLNAME        razzball name only

    downloadUrl = 'https://www.smartfantasybaseball.com/PLAYERIDMAPCSV'

    headingMapping = {
        'playerID': 'lahman',
        'IDFANGRAPHS': 'fangraphs',
        'OTTONEUID': 'ottoneu',
        'MLBID': 'mlb',
        'CBSID': 'cbs',
        'RETROID': 'retrosheet',
        'BREFID': 'baseball_reference',
        'NFBCID': 'nfbc',
        'ESPNID': 'espn',
        'KFFLNAME': None,
        'DAVENPORTID': None,
        'BPID': 'baseball_prospectus',
        'YAHOOID': 'yahoo',
        'MSTRBLLNAME': None,
        'FANTPROSNAME': None,
        'ROTOWIREID': 'rotowire',
        'FANDUELID': 'fanduel',
        'DRAFTKINGSNAME': None,
        'HQID': None,
        'FANTRAXID': 'fantrax',
        'RAZZBALLNAME': None
    }

    def get(self):
        '''
            Download mapping data and format as needed.

            return a dataframe of the mapping data.
        '''

        # request data from Github repo
        #
        #
        #   had to add a user agent, since default python requests agent is blocked
        response = requests.get(
            self.downloadUrl,
            headers = {
                "Accept-Encoding": "gzip, deflate, br",
                "User-Agent": "Chrome/80.0.3987.132"
            }
        )

        # load response data into a dataframe
        #
        smartFile = io.StringIO(response.text)
        smartFile.seek(0)
        df = pd.read_csv(smartFile, dtype='string', header=0, usecols=self.headingMapping.keys())

        # rename columns
        #
        df.rename(columns=self.headingMapping, inplace=True)

        return df

class CrunchtimeBaseball:
    '''
        CrunchTimeBaseball.com ID mapping.
    '''

    # Notes on source data.
    #
    #   mlb_id              MLB.com
    #   mlb_name
    #   mlb_pos
    #   mlb_team
    #   bp_id               baseball prospectus
    #   bref_id             baseball reference
    #   cbs_id              CBS
    #   cbs_pos
    #   espn_id             ESPN
    #   espn_pos
    #   fg_id               Fangraphs
    #   fg_pos
    #   lahman_id           Lahman database
    #   nfbc_id             NFBC
    #   nfbc_pos
    #   retro_id            retrosheet
    #   yahoo_id            yahoo
    #   ottoneu_id          ottoneu
    #   ottoneu_pos
    #   rotowire_id         rotowire
    #   rotowire_pos
    #

    downloadUrl = 'http://crunchtimebaseball.com/master.csv'

    headingMapping = {
        'mlb_id': 'mlb',
        'mlb_name': None,
        'mlb_pos': None,
        'mlb_team': None,
        'bp_id': 'baseball_prospectus',
        'bref_id': 'baseball_reference',
        'cbs_id': 'cbs',
        'cbs_pos': None,
        'espn_id': 'espn',
        'espn_pos': None,
        'fg_id': 'fangraphs',
        'fg_pos': None,
        'lahman_id': 'lahman',
        'nfbc_id': 'nfbc',
        'nfbc_pos': None,
        'retro_id': 'retrosheet',
        'yahoo_id': 'yahoo',
        'ottoneu_id': 'ottoneu',
        'ottoneu_pos': None,
        'rotowire_id': 'rotowire',
        'rotowire_pos': None
    }

    def get(self):
        '''
            Download mapping data and format as needed.

            return a dataframe of the mapping data.
        '''

        # request data from Github repo
        #
        #
        #   had to add a user agent, since default python requests agent is blocked
        response = requests.get(
            self.downloadUrl,
            headers = {
                "Accept-Encoding": "gzip, deflate, br"
            }
        )

        # load response data into a dataframe
        #
        crunchFile = io.StringIO(response.text)
        crunchFile.seek(0)
        df = pd.read_csv(crunchFile, dtype='string', header=0, usecols=self.headingMapping.keys())

        # rename columns
        #
        df.rename(columns=self.headingMapping, inplace=True)

        return df

class MySportsFeeds:
    '''
        download id mappings from mysportsfeeds api
    '''

    # Notes on source data.
        #
        #    player object has an externalMappings attribute
        #    "externalMappings":[{"source":"MLB.com","id":592094}]
        #
        #    returns dataframe with columns of [msf_id, mlb.com]
        #
        #
        #    "lastUpdatedOn": "2020-07-29T14:01:33.073Z",
        #    "players": [
        #        {
        #            "player": {
        #                "id": 10277,
        #                "firstName": "Fernando",
        #                "lastName": "Abad",
        #                "primaryPosition": "P",
        #                "alternatePositions": [],
        #                "jerseyNumber": 50,
        #                "currentTeam": {
        #                    "id": 126,
        #                    "abbreviation": "WAS"
        #                },
        #                "currentRosterStatus": "ASSIGNED_TO_MINORS",
        #                "currentInjury": null,
        #                "height": "6'2\"",
        #                "weight": 235,
        #                "birthDate": "1985-12-17",
        #                "age": 34,
        #                "birthCity": "La Romana",
        #                "birthCountry": "Dominican Republic",
        #                "rookie": false,
        #                "highSchool": null,
        #                "college": null,
        #                "handedness": {
        #                    "bats": "L",
        #                    "throws": "L"
        #                },
        #                "officialImageSrc": "http://mlb.mlb.com/mlb/images/players/head_shot/472551.jpg",
        #                "socialMediaAccounts": [],
        #                "currentContractYear": null,
        #                "drafted": null,
        #                "externalMappings": [
        #                    {
        #                        "source": "MLB.com",
        #                        "id": 472551
        #                    }
        #                ]
        #            },
        #            "teamAsOfDate": {
        #                "id": 126,
        #                "abbreviation": "WAS"
        #            }
        #        },
        #

    headingMapping = {
        'msf_id': 'mysportsfeeds',
        'mlb.com': 'mlb'
    }

    def __init__(self, apiKey, season):
        '''
            apiKey      The mysportsfeeds API key.
                        can also be defined with MYSPORTSFEEDS_API_KEY or
                        MYSPORTSFEEDS_API_KEY_FILE.
            season      integer season to download player data from.
        '''

        self.season = season

        if apiKey is None:
            if os.environ.get('MYSPORTSFEEDS_API_KEY'):
                self.apiKey = os.environ.get('MYSPORTSFEEDS_API_KEY')

            elif os.environ.get('MYSPORTSFEEDS_API_KEY_FILE'):
                with open(os.environ.get('MYSPORTSFEEDS_API_KEY_FILE'), 'r') as f:
                    self.apiKey = str(f.read()).strip()

            else:
                raise ValueError("API key must be given either in constructor, or with ENV variables.")

        else:
            self.apiKey = apiKey

    def get(self):
        '''
            Download mapping data and format as needed.

            return a dataframe of the mapping.
        '''

        msf = ohmysportsfeedspy.MySportsFeeds(version="2.1")
        msf.authenticate(self.apiKey, "MYSPORTSFEEDS")
        output = msf.msf_get_data(
            league='mlb',
            season='{0}-regular'.format(self.season),
            feed='players',
            format='json',
            force=True
        )

        foundMappings = []

        for player in output['players']:

            playerID = player['player']['id']
            externalMappings = player['player']['externalMappings']

            mapDict = dict()
            mapDict['msf_id'] = playerID
            for mapping in externalMappings:
                src = mapping.get('source')

                if src:
                    mapDict[src.lower()] = str(mapping.get('id'))

            foundMappings.append(mapDict)

        df = pd.DataFrame(foundMappings, dtype='object')
        df.rename(columns=self.headingMapping, inplace=True)
        return df

class SportsDataIo:
    '''
    Pull in ID mappings from sportsdata.io API.

    Can join to main dataset using MLBAMID.
    '''

    # example of data in Active Players API endpoint
    #
        #{
        #    'PlayerID': 10000249,
        #    'SportsDataID': '',
        #    'Status': '60 Day Injury List',
        #    'TeamID': 25,
        #    'Team': 'BOS',
        #    'Jersey': 41,
        #    'PositionCategory': 'P',
        #    'Position': 'SP',
        #    'MLBAMID': 519242,
        #    'FirstName': 'Chris',
        #    'LastName': 'Sale',
        #    'BatHand': 'L',
        #    'ThrowHand': 'L',
        #    'Height': 78,
        #    'Weight': 183,
        #    'BirthDate': '1989-03-30T00:00:00',
        #    'BirthCity': 'Lakeland',
        #    'BirthState': 'FL',
        #    'BirthCountry': 'USA',
        #    'HighSchool': None,
        #    'College': 'Florida Gulf Coast',
        #    'ProDebut': '2010-08-06T00:00:00',
        #    'Salary': None,
        #    'PhotoUrl': 'https://s3-us-west-2.amazonaws.com/static.fantasydata.com/headshots/mlb/low-res/10000249.png',
        #    'SportRadarPlayerID': '66c43b7f-760a-446c-9f50-671915570227',
        #    'RotoworldPlayerID': 6319,
        #    'RotoWirePlayerID': 11438,
        #    'FantasyAlarmPlayerID': 100433,
        #    'StatsPlayerID': 392121,
        #    'SportsDirectPlayerID': 107801,
        #    'XmlTeamPlayerID': 8322,
        #    'InjuryStatus': 'Scrambled',
        #    'InjuryBodyPart': 'Scrambled',
        #    'InjuryStartDate': '2021-02-24T00:00:00',
        #    'InjuryNotes': 'Scrambled',
        #    'FanDuelPlayerID': 12477,
        #    'DraftKingsPlayerID': 392121,
        #    'YahooPlayerID': 8780,
        #    'UpcomingGameID': 64103,
        #    'FanDuelName': 'Chris Sale',
        #    'DraftKingsName': 'Chris Sale',
        #    'YahooName': 'Chris Sale',
        #    'GlobalTeamID': 10000025,
        #    'FantasyDraftName': 'Chris Sale',
        #    'FantasyDraftPlayerID': 392121,
        #    'Experience': '10',
        #    'UsaTodayPlayerID': 8253448,
        #    'UsaTodayHeadshotUrl':
        #    'http://cdn.usatsimg.com/api/download/?imageID=15754012',
        #    'UsaTodayHeadshotNoBackgroundUrl':
        #    'http://cdn.usatsimg.com/api/download/?imageID=15754247',
        #    'UsaTodayHeadshotUpdated': '2021-03-19T08:57:58',
        #    'UsaTodayHeadshotNoBackgroundUpdated': '2021-03-19T09:33:26'
        #}

    headingMap = {
        'PlayerID': 'sportsdataio',
        'MLBAMID': 'mlb',
        'RotoWirePlayerID': 'rotowire',
        'FanDuelPlayerID': 'fanduel',
        'DraftKingsPlayerID': 'draftkings',
        'YahooPlayerID': 'yahoo'
    }

    def __init__(self, apiKey=None):
        '''
        apiKey      the sportsdataio api key to use for authentication.
                    can also be defined with SPORTSDATAIO_API_KEY or
                        SPORTSDATAIO_API_KEY_FILE.
        '''

        self.apiKey = apiKey

        if apiKey is None:
            if os.environ.get('SPORTSDATAIO_API_KEY'):
                self.apiKey = os.environ.get('SPORTSDATAIO_API_KEY')

            elif os.environ.get('SPORTSDATAIO_API_KEY_FILE'):
                with open(os.environ.get('SPORTSDATAIO_API_KEY_FILE'), 'r') as f:
                    self.apiKey = str(f.read()).strip()

            else:
                raise ValueError("API key must be given either in constructor, or with ENV variables.")

    def get(self):
        '''
        Get the mappings. Return a dataframe with the mappings for all active players.
        '''

        req = requests.get(
            'https://api.sportsdata.io/v3/mlb/scores/json/Players',
            headers={
                'Ocp-Apim-Subscription-Key': self.apiKey
            }
        )

        if req.status_code != 200:
            raise RuntimeError("Request to SportsData.io endpoing was unsuccessful.")

        data = req.json()

        mappings = pd.DataFrame(data, dtype='object')
        mappings = mappings.loc[:, self.headingMap.keys()]
        return mappings.rename(columns=self.headingMap)


# sportsipy (https://sportsreference.readthedocs.io/en/stable/nfl.html#module-sportsipy.nfl.teams)
# baseball-reference
#



def cli_chadwick(args):
    '''
        chadwick command

        download data from Chadwick Bureau Register
    '''

    #args.outFile

    scraper = ChadwickBureauRegister()
    mappings = scraper.get()
    mappings.to_csv(
        args.outFile,
        header=True,
        index=False
    )

def cli_lahman(args):
    '''
        lahman command

        download data from lahman database
    '''

    #args.outFile

    scraper = LahmanDatabase()
    mappings = scraper.get()
    mappings.to_csv(
        args.outFile,
        header=True,
        index=False
    )

def cli_smartfantasybaseball(args):
    '''
        smartfantasybaseball command

        download mapping data from smartfantasybaseball
    '''

    #args.outFile

    scraper = SmartFantasyBaseball()
    mappings = scraper.get()
    mappings.to_csv(
        args.outFile,
        header=True,
        index=False
    )

def cli_crunchtimebaseball(args):
    '''
        crunchtimebaseball command

        download mapping data from crunchtimebaseball
    '''

    #args.outFile

    scraper = CrunchtimeBaseball()
    mappings = scraper.get()
    mappings.to_csv(
        args.outFile,
        header=True,
        index=False
    )

def cli_mysportsfeeds(args):
    '''
        mysportsfeeds command

        download mapping data from mysportsfeeds
    '''

    #args.outFile
    #args.season
    #args.mysportsfeedsApiKey

    scraper = MySportsFeeds(args.mysportsfeedsApiKey, args.season)
    mappings = scraper.get()
    mappings.to_csv(
        args.outFile,
        header=True,
        index=False
    )

def cli_sportsdataio(args):
    '''
        sportsdataio command

        download mapping data from sportsdata.io
    '''

    #args.outFile
    #args.sportsdataioApiKey

    scraper = SportsDataIo(args.sportsdataioApiKey)
    mappings = scraper.get()
    mappings.to_csv(
        args.outFile,
        header=True,
        index=False
    )

def _add_argument_helper(parser, argumentParameters):
    '''
        add an argument to a parser from a dictionary of
        definitions.
    '''

    params = argumentParameters.copy()
    parser.add_argument(params.pop('name'), **params)

def cli_arguments(parser):
    '''
        add parsers for each command. parser can be a ArgumentParser or a subparsers object
        created by add_subparsers().
    '''

    # define command line options so they can be reused.
    #
    outFile = {
        'name': '--outFile',
        'action': 'store',
        'help': 'The path to the created csv file.'
    }

    season = {
        'name': '--season',
        'action': 'store',
        'help': 'The season to target for download.'
    }

    mysportsfeedsApiKey = {
        'name': '--mysportsfeedsApiKey',
        'action': 'store',
        'help': 'The API key used to access the MySportsFeeds API. '
                'Can also be defined via environment variables MYSPORTSFEEDS_API_KEY or MYSPORTSFEEDS_API_KEY_FILE.'
    }

    sportsdataioApiKey = {
        'name': '--sportsdataioApiKey',
        'action': 'store',
        'help': 'The API key used to access the SportsData.io API. '
                'Can also be defined via environment variables SPORTSDATAIO_API_KEY or SPORTSDATAIO_API_KEY_FILE.'
    }


    # add the various commands and their arguments
    #

    chadwick = parser.add_parser('chadwick')
    _add_argument_helper(chadwick, outFile)
    chadwick.set_defaults(func=cli_chadwick)

    lahman = parser.add_parser('lahman')
    _add_argument_helper(lahman, outFile)
    lahman.set_defaults(func=cli_lahman)

    smartfantasybaseball = parser.add_parser('smartfantasybaseball')
    _add_argument_helper(smartfantasybaseball, outFile)
    smartfantasybaseball.set_defaults(func=cli_smartfantasybaseball)

    crunchtimebaseball = parser.add_parser('crunchtimebaseball')
    _add_argument_helper(crunchtimebaseball, outFile)
    crunchtimebaseball.set_defaults(func=cli_crunchtimebaseball)

    mysportsfeeds = parser.add_parser('mysportsfeeds')
    _add_argument_helper(mysportsfeeds, outFile)
    _add_argument_helper(mysportsfeeds, season)
    _add_argument_helper(mysportsfeeds, mysportsfeedsApiKey)
    mysportsfeeds.set_defaults(func=cli_mysportsfeeds)

    sportsdataio = parser.add_parser('sportsdataio')
    _add_argument_helper(sportsdataio, outFile)
    _add_argument_helper(sportsdataio, sportsdataioApiKey)
    sportsdataio.set_defaults(func=cli_sportsdataio)

